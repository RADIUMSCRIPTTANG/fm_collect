# CMake generated Testfile for 
# Source directory: /home/radium/Desktop/GR_Learning/gr-radium/python
# Build directory: /home/radium/Desktop/GR_Learning/gr-radium/build/python
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(qa_msg_sink_01 "/bin/sh" "/home/radium/Desktop/GR_Learning/gr-radium/build/python/qa_msg_sink_01_test.sh")
add_test(qa_data_sink_f_04 "/bin/sh" "/home/radium/Desktop/GR_Learning/gr-radium/build/python/qa_data_sink_f_04_test.sh")
add_test(qa_data_sink_f_05 "/bin/sh" "/home/radium/Desktop/GR_Learning/gr-radium/build/python/qa_data_sink_f_05_test.sh")
