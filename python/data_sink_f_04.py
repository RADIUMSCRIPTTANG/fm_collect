#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2021 gr-radium author.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#


import numpy as np
from gnuradio import gr
from gnuradio import qtgui
from enum import Enum
import pickle
import pmt

class STATUS(Enum):
    WAITING = 1
    COLLECTING = 2


class data_sink_f_04(gr.sync_block):
    """
    docstring for block data_sink_f_04
    """

    def __init__(self, vlen, output, n_samples_per_attempt, n_attempt, n_wait, start_freq, end_freq, bandwidth):
        gr.sync_block.__init__(self,
                               name="data_sink_f_04",
                               in_sig=[(np.float32, vlen), ],
                               out_sig=None)

        self.output_filepath = "/home/radium/Desktop/jupyter_project/USRP_DATA/{}.pkl".format(output)
        self.n_wait = n_wait
        self.n_samples_per_attempt = n_samples_per_attempt
        self.n_attempt = n_attempt
        self.start_freq = start_freq
        self.end_freq = end_freq
        self.bandwidth = bandwidth
        self.current_center_freq = self.start_freq + self.bandwidth / 2

        self.message_port_register_out(pmt.intern("freq"))

        self.collect = []
        self.tmp_collect = []
        self.status = STATUS.WAITING

        self.current_attempt = 0
        self.current_sample = 0
        self.current_waiting = 0
        self.current_freq_sample = 0

    def reset_collect(self):
        print("attempt {} has finished".format(self.current_attempt))
        self.collect.append(np.array(self.tmp_collect))
        self.current_sample = 0
        self.current_attempt += 1
        self.current_waiting = 0
        self.status = STATUS.WAITING
        self.tmp_collect = []
        if self.current_attempt == self.n_attempt:
            with open(self.output_filepath, "wb") as f:
                pickle.dump(self.collect, f)
            print("collected")


    def work(self, input_items, output_items):

        # self.message_port_pub(pmt.intern("freq"), pmt.intern("100e6"))
        in0 = input_items[0]
        if self.current_attempt >= self.n_attempt:
            return len(in0)
        if self.status == STATUS.WAITING:
            self.current_waiting += 1
            if self.current_waiting > self.n_wait:
                self.status = STATUS.COLLECTING
            return len(in0)

        if self.status == STATUS.COLLECTING:
            for line in in0:
                self.tmp_collect.append(line)
                self.current_sample += 1

        if self.current_sample >= self.n_samples_per_attempt:
            self.reset_collect()

        return len(in0)
