#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2021 gr-radium author.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#


import numpy as np
from gnuradio import gr
import pickle

class data_sink(gr.sync_block):
    """
    docstring for block data_sink_c
    """

    def __init__(self, sample_rate, output_file):
        gr.sync_block.__init__(self,
                               name="data_sink",
                               in_sig=[np.complex64, ],
                               out_sig=None)
        self.sample_num = 1000
        self.collect = []
        self.collected_num = 0
        self.saved = False
        self.output_file = output_file
        print("Sink created")
    def work(self, input_items, output_items):
    #     in0 = input_items[0]
    #     self.collect.append(in0)
    #     self.collected_num += 1
    #     if self.collected_num >= self.sample_num and not self.saved:
    #         with open("/home/radium/Desktop/jupyter_project/USRP_DATA/{}.pkl".format(self.output_file), "wb") as f:
    #             pickle.dump(self.collect, f)
    #         self.saved = True
    #         print("collected")

        # <+signal processing here+>
        return len(input_items[0])
