#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2021 gr-radium author.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#


import numpy as np
from gnuradio import gr
import pickle

class data_sink_03(gr.sync_block):
    """
    docstring for block data_sink_03
    """

    def __init__(self, filename, vlen, sample_num, wait, total_sampling):
        gr.sync_block.__init__(self,
            name="data_sink_03",
            in_sig=[(np.float32, vlen), ],
            out_sig=None)
        self.sample_num = sample_num
        self.collect = []
        self.collected_num = 0
        self.saved = False
        self.output_file = filename
        self.wait = wait
        self.total_sampling = total_sampling
        self.tmp = []
        self.status = 0 # wait
        print("Sink created")

    def work(self, input_items, output_items):
        in0 = input_items[0]

        # for line in in0:
        #     self.tmp.append(line)
        # self.collected_num += 1
        # if self.collected_num >= self.sample_num and not self.saved:
        #     with open("/home/radium/Desktop/jupyter_project/USRP_DATA/{}.pkl".format(self.output_file), "wb") as f:
        #         pickle.dump(self.collect, f)
        #     self.saved = True
        #     print("collected")
        return len(input_items[0])

