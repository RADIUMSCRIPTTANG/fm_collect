/* -*- c++ -*- */

#define RADIUM_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "radium_swig_doc.i"

%{
%}

