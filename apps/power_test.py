#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Power measurement
# GNU Radio version: 3.8.2.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time
from gnuradio.qtgui import Range, RangeWidget
import radium

from gnuradio import qtgui

class power_test(gr.top_block, Qt.QWidget):

    def __init__(self, fft_size=1000, n_fft=100, start_freq=90e6, step_freq=0.1e6, stop_freq=110e6):
        gr.top_block.__init__(self, "Power measurement")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Power measurement")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "power_test")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Parameters
        ##################################################
        self.fft_size = fft_size
        self.n_fft = n_fft
        self.start_freq = start_freq
        self.step_freq = step_freq
        self.stop_freq = stop_freq

        ##################################################
        # Variables
        ##################################################
        self.window_power = window_power = 263897
        self.samp_rate = samp_rate = 2e6
        self.ref_scale = ref_scale = 2
        self.n_step = n_step = 4
        self.n_bins = n_bins = 768
        self.freq_resolution = freq_resolution = int(50e3)
        self.center_freq_range = center_freq_range = 50
        self.center_freq = center_freq = 100e6

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
        )
        self.uhd_usrp_source_0.set_center_freq(center_freq, 0)
        self.uhd_usrp_source_0.set_gain(0, 0)
        self.uhd_usrp_source_0.set_antenna('TX/RX', 0)
        self.uhd_usrp_source_0.set_bandwidth(samp_rate, 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        # No synchronization enforced.
        self.radium_data_sink_f_05_0 = radium.data_sink_f_05(fft_size, 'test000', 2000, 3, 5000, 97e6, 103e6, samp_rate)
        self.qtgui_sink_x_0 = qtgui.sink_c(
            1024, #fftsize
            firdes.WIN_BLACKMAN_hARRIS, #wintype
            center_freq, #fc
            samp_rate, #bw
            "", #name
            True, #plotfreq
            True, #plotwaterfall
            True, #plottime
            True #plotconst
        )
        self.qtgui_sink_x_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_win = sip.wrapinstance(self.qtgui_sink_x_0.pyqwidget(), Qt.QWidget)

        self.qtgui_sink_x_0.enable_rf_freq(False)

        self.top_grid_layout.addWidget(self._qtgui_sink_x_0_win)
        self.fft_vxx_0 = fft.fft_vcc(fft_size, True, window.blackmanharris(fft_size), True, 1)
        self._center_freq_range_range = Range(0, 100, 1, 50, 200)
        self._center_freq_range_win = RangeWidget(self._center_freq_range_range, self.set_center_freq_range, 'center_freq_range', "counter_slider", float)
        self.top_grid_layout.addWidget(self._center_freq_range_win)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, fft_size)
        self.blocks_message_debug_0 = blocks.message_debug()
        self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(fft_size)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.qtgui_sink_x_0, 'freq'), (self.blocks_message_debug_0, 'print'))
        self.msg_connect((self.radium_data_sink_f_05_0, 'freq'), (self.blocks_message_debug_0, 'print'))
        self.msg_connect((self.radium_data_sink_f_05_0, 'freq'), (self.qtgui_sink_x_0, 'freq'))
        self.connect((self.blocks_complex_to_mag_squared_0, 0), (self.radium_data_sink_f_05_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.blocks_complex_to_mag_squared_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.qtgui_sink_x_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "power_test")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_fft_size(self):
        return self.fft_size

    def set_fft_size(self, fft_size):
        self.fft_size = fft_size

    def get_n_fft(self):
        return self.n_fft

    def set_n_fft(self, n_fft):
        self.n_fft = n_fft

    def get_start_freq(self):
        return self.start_freq

    def set_start_freq(self, start_freq):
        self.start_freq = start_freq

    def get_step_freq(self):
        return self.step_freq

    def set_step_freq(self, step_freq):
        self.step_freq = step_freq

    def get_stop_freq(self):
        return self.stop_freq

    def set_stop_freq(self, stop_freq):
        self.stop_freq = stop_freq

    def get_window_power(self):
        return self.window_power

    def set_window_power(self, window_power):
        self.window_power = window_power

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_sink_x_0.set_frequency_range(self.center_freq, self.samp_rate)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_bandwidth(self.samp_rate, 0)

    def get_ref_scale(self):
        return self.ref_scale

    def set_ref_scale(self, ref_scale):
        self.ref_scale = ref_scale

    def get_n_step(self):
        return self.n_step

    def set_n_step(self, n_step):
        self.n_step = n_step

    def get_n_bins(self):
        return self.n_bins

    def set_n_bins(self, n_bins):
        self.n_bins = n_bins

    def get_freq_resolution(self):
        return self.freq_resolution

    def set_freq_resolution(self, freq_resolution):
        self.freq_resolution = freq_resolution

    def get_center_freq_range(self):
        return self.center_freq_range

    def set_center_freq_range(self, center_freq_range):
        self.center_freq_range = center_freq_range

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.qtgui_sink_x_0.set_frequency_range(self.center_freq, self.samp_rate)
        self.uhd_usrp_source_0.set_center_freq(self.center_freq, 0)




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "--fft-size", dest="fft_size", type=intx, default=1000,
        help="Set fft_size [default=%(default)r]")
    parser.add_argument(
        "--n-fft", dest="n_fft", type=intx, default=100,
        help="Set n_fft [default=%(default)r]")
    parser.add_argument(
        "--start-freq", dest="start_freq", type=eng_float, default="90.0M",
        help="Set start_freq [default=%(default)r]")
    parser.add_argument(
        "--step-freq", dest="step_freq", type=eng_float, default="100.0k",
        help="Set step_freq [default=%(default)r]")
    parser.add_argument(
        "--stop-freq", dest="stop_freq", type=eng_float, default="110.0M",
        help="Set stop_freq [default=%(default)r]")
    return parser


def main(top_block_cls=power_test, options=None):
    if options is None:
        options = argument_parser().parse_args()

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(fft_size=options.fft_size, n_fft=options.n_fft, start_freq=options.start_freq, step_freq=options.step_freq, stop_freq=options.stop_freq)

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()

    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()

if __name__ == '__main__':
    main()
