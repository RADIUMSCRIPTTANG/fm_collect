INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_RADIUM radium)

FIND_PATH(
    RADIUM_INCLUDE_DIRS
    NAMES radium/api.h
    HINTS $ENV{RADIUM_DIR}/include
        ${PC_RADIUM_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    RADIUM_LIBRARIES
    NAMES gnuradio-radium
    HINTS $ENV{RADIUM_DIR}/lib
        ${PC_RADIUM_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/radiumTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(RADIUM DEFAULT_MSG RADIUM_LIBRARIES RADIUM_INCLUDE_DIRS)
MARK_AS_ADVANCED(RADIUM_LIBRARIES RADIUM_INCLUDE_DIRS)
