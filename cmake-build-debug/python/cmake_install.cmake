# Install script for directory: /home/radium/Desktop/GR_Learning/gr-radium/python

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python3/dist-packages/radium" TYPE FILE FILES
    "/home/radium/Desktop/GR_Learning/gr-radium/python/__init__.py"
    "/home/radium/Desktop/GR_Learning/gr-radium/python/data_sink.py"
    "/home/radium/Desktop/GR_Learning/gr-radium/python/data_sink_03.py"
    "/home/radium/Desktop/GR_Learning/gr-radium/python/msg_sink_01.py"
    "/home/radium/Desktop/GR_Learning/gr-radium/python/data_sink_f_04.py"
    "/home/radium/Desktop/GR_Learning/gr-radium/python/data_sink_f_05.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python3/dist-packages/radium" TYPE FILE FILES
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/__init__.pyc"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink.pyc"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink_03.pyc"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/msg_sink_01.pyc"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink_f_04.pyc"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink_f_05.pyc"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/__init__.pyo"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink.pyo"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink_03.pyo"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/msg_sink_01.pyo"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink_f_04.pyo"
    "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/data_sink_f_05.pyo"
    )
endif()

