# CMake generated Testfile for 
# Source directory: /home/radium/Desktop/GR_Learning/gr-radium/python
# Build directory: /home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(qa_msg_sink_01 "/bin/sh" "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/qa_msg_sink_01_test.sh")
set_tests_properties(qa_msg_sink_01 PROPERTIES  _BACKTRACE_TRIPLES "/usr/lib/x86_64-linux-gnu/cmake/gnuradio/GrTest.cmake;122;add_test;/home/radium/Desktop/GR_Learning/gr-radium/python/CMakeLists.txt;50;GR_ADD_TEST;/home/radium/Desktop/GR_Learning/gr-radium/python/CMakeLists.txt;0;")
add_test(qa_data_sink_f_04 "/bin/sh" "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/qa_data_sink_f_04_test.sh")
set_tests_properties(qa_data_sink_f_04 PROPERTIES  _BACKTRACE_TRIPLES "/usr/lib/x86_64-linux-gnu/cmake/gnuradio/GrTest.cmake;122;add_test;/home/radium/Desktop/GR_Learning/gr-radium/python/CMakeLists.txt;51;GR_ADD_TEST;/home/radium/Desktop/GR_Learning/gr-radium/python/CMakeLists.txt;0;")
add_test(qa_data_sink_f_05 "/bin/sh" "/home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug/python/qa_data_sink_f_05_test.sh")
set_tests_properties(qa_data_sink_f_05 PROPERTIES  _BACKTRACE_TRIPLES "/usr/lib/x86_64-linux-gnu/cmake/gnuradio/GrTest.cmake;122;add_test;/home/radium/Desktop/GR_Learning/gr-radium/python/CMakeLists.txt;52;GR_ADD_TEST;/home/radium/Desktop/GR_Learning/gr-radium/python/CMakeLists.txt;0;")
