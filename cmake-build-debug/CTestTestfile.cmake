# CMake generated Testfile for 
# Source directory: /home/radium/Desktop/GR_Learning/gr-radium
# Build directory: /home/radium/Desktop/GR_Learning/gr-radium/cmake-build-debug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("include/radium")
subdirs("lib")
subdirs("apps")
subdirs("docs")
subdirs("swig")
subdirs("python")
subdirs("grc")
